package phase4;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import utils.JoinPredicate;
import utils.QuantileSizeSingleton;
import utils.Record;

public class Phase4Driver {
	public static void run(String[] args, String phase2Path, String phase3Path, String input, String outDir, QuantileSizeSingleton size, int nRegions, JoinPredicate pred) throws Exception{
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		FileSystem fs = FileSystem.get(conf);
		Path outPath = new Path(outDir);
		
		if(fs.exists(outPath)) 
			fs.delete(outPath, true);
		conf.setInt("rQuantiles", size.getR());
		conf.setInt("sQuantiles", size.getS());
		conf.set("predicate", pred.toString());

		// Region info
		DistributedCache.addCacheFile(new Path(phase3Path + "/regions").toUri(), conf);
		// Bucket info
		FileStatus[] phase2Files = fs.listStatus(new Path(phase2Path));
		for(FileStatus fstat : phase2Files)
			if(fstat.getPath().toString().contains("part-r-")) 
				DistributedCache.addCacheFile(fstat.getPath().toUri(), conf);

		Job job = Job.getInstance(conf, "Phase4: Actual join");
		job.setJarByClass(phase4.Phase4Driver.class);
		job.setMapperClass(Phase4Mapper.class);
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(Record.class);
		job.setReducerClass(Phase4Reducer.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);
		
		job.setNumReduceTasks(nRegions);

		FileInputFormat.addInputPath(job, new Path(input));
		FileOutputFormat.setOutputPath(job, outPath);

		if(!job.waitForCompletion(true)) return;
	}
}
