package phase4;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import utils.BucketKeyPair;
import utils.Quantile;
import utils.Record;
import utils.Region;

/**
 * Map output
 * key   : region
 * value : record
 */
public class Phase4Mapper extends Mapper<Object, Text, IntWritable, Record>{
	private ArrayList<Region> regions = new ArrayList<Region>();
	private Quantile[] rQuantile;
	private Quantile[] sQuantile;
	
	private ArrayList<Integer> getRegions(Record record){
		ArrayList<Integer> regionList = new ArrayList<Integer>();
		Quantile[] quantile = null;
		int min, max;
		min = max = 0;
		
		quantile = record.getTable() == 'R'? rQuantile : sQuantile;
		// find bucket
		for(int i = 0; i < quantile.length; i++){
			max = min + quantile[i].getCount() - 1; // valid from 0
			if(record.getValue() <= quantile[i].getMax()){
				// randomly choose index within the bucket
				Random rand = new Random();
				int index = min + rand.nextInt(max-min + 1);
				// send to regions that crosses this index
				for(int j = 0; j < regions.size(); j++){
					if(record.getTable() == 'R'){
						if (regions.get(j).getColInit() <= index && regions.get(j).getColEnd() >= index)
							regionList.add(j);
					}
					else{
						if (regions.get(j).getRowInit() <= index && regions.get(j).getRowEnd() >= index)
							regionList.add(j);
					}
				}
				break;
			}
			min = max + 1;// for next iteration
		}
		return regionList;
	}
	
	@Override
	protected void setup(Context context)	throws IOException, InterruptedException {
		Configuration conf = context.getConfiguration();
		URI[] caches = DistributedCache.getCacheFiles(conf);
		FileSystem fs = FileSystem.get(conf);
		int rQuantiles = conf.getInt("rQuantiles", -1);
		int sQuantiles = conf.getInt("sQuantiles", -1);
		rQuantile = new Quantile[rQuantiles];
		sQuantile = new Quantile[sQuantiles];
		
		// read region info from phase3
		SequenceFile.Reader reader = new SequenceFile.Reader(fs,  new Path(caches[0].toString()), conf);
		NullWritable key = NullWritable.get();
		Region r = new Region();
		while(reader.next(key, r))
			regions.add(new Region(r));
		reader.close();
		
		// read quantiles info from phase2
		for(int i = 1; i < caches.length; i++){
			reader = new SequenceFile.Reader(fs,  new Path(caches[i].toString()), conf);
			BucketKeyPair bucketKey = new BucketKeyPair();
			Quantile val = new Quantile();
			
			while(reader.next(bucketKey, val)){
				if(bucketKey.getTable() == 'R'){
					rQuantile[bucketKey.getBucketId()] = new Quantile(val);
				}else{
					sQuantile[bucketKey.getBucketId()] = new Quantile(val);
				}
			}
			reader.close();
		}
	}
	
	@Override
	protected void map(Object key, Text value,Context context) throws IOException, InterruptedException {
		Record record = new Record(value.toString());
		ArrayList<Integer> reducers = getRegions(record);
		for(Integer r : reducers){
			context.write(new IntWritable(r), record);
		}
	}	
}
