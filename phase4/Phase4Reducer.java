package phase4;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import utils.JoinPredicate;
import utils.Record;

public class Phase4Reducer extends Reducer<IntWritable, Record, NullWritable, Text> {
	private static final NullWritable outKey = NullWritable.get();
	private JoinPredicate predicate;
	
	@Override
	protected void setup(Context context){
		Configuration conf = context.getConfiguration();
		predicate = JoinPredicate.valueOf(conf.get("predicate"));
	}
	
	@Override
	public void reduce(IntWritable key, Iterable<Record> values, Context context) throws IOException, InterruptedException{
		ArrayList<Record> rRecords = new ArrayList<Record>();
		ArrayList<Record> sRecords = new ArrayList<Record>();
		for(Record v : values){
			if(v.getTable() == 'R')
				rRecords.add(new Record(v));
			else
				sRecords.add(new Record(v));
		}
		for(Record r : rRecords){
			for(Record s : sRecords){
				if(predicate == JoinPredicate.EQUI){
					if(r.getValue() == s.getValue())
						context.write(outKey, new Text(r.toString()+"\t"+s.toString()));
				}
				else{
					if(r.getValue() <= s.getValue())
						context.write(outKey, new Text(r.toString()+"\t"+s.toString()));
				}
			}
		}
	}
}
