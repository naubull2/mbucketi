package phase3;

import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;

import utils.BucketKeyPair;
import utils.JoinPredicate;
import utils.Quantile;
import utils.QuantileSizeSingleton;
import utils.Region;

public class Phase3Driver {

	public static void run(String phase2Dir, String outputDir, QuantileSizeSingleton size, int maxRegions, JoinPredicate predicate) throws Exception{
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		FileStatus[] status = fs.listStatus(new Path(phase2Dir));
		
		int rSize = 0;
		int sSize = 0;
		// Count quantiles
		for(FileStatus fstat : status){
			if(fstat.getPath().getName().contains("part-r")){
				SequenceFile.Reader reader = new SequenceFile.Reader(fs, fstat.getPath(), conf);
				BucketKeyPair key = new BucketKeyPair();
				Quantile val = new Quantile();
				while(reader.next(key, val)){
					if(key.getTable() == 'R'){
						rSize++;
					}else{
						sSize++;
					}
				}
			}
		}
		size.setR(rSize);
		size.setS(sSize);
		Quantile[] rQuantile = new Quantile[rSize];
		Quantile[] sQuantile = new Quantile[sSize];
		// Read histogram
		for(FileStatus fstat : status){
			if(fstat.getPath().getName().contains("part-r")){
				SequenceFile.Reader reader = new SequenceFile.Reader(fs, fstat.getPath(), conf);
				BucketKeyPair key = new BucketKeyPair();
				Quantile val = new Quantile();
				
				while(reader.next(key, val)){
					if(key.getTable() == 'R'){
						rQuantile[key.getBucketId()] = new Quantile(val);
					}else{
						sQuantile[key.getBucketId()] = new Quantile(val);
					}
				}
			}
		}
		
		// Process matrix
		// 1. compute candidate cells according to the join predicate
		/*
		 * Classes needed : Matrix - receives R,S quantile info and build matrix upon join predicate
		 *                  ColumnBucket - saves the range of candidate cells in each column bucket
		 */
		// R tables are considered column of the join matrix
		JoinMatrix matrix = new JoinMatrix(rQuantile, sQuantile, predicate);
		matrix.buildColumnBucket();
		//matrix.printMatrix(100, 100);
		
		/* 
		 * Run M-Bucket-I algorithm
		 */
		ArrayList<Region> regions = matrix.MBucketI(maxRegions);
		//////////////////////////
		// Print Regions
		System.out.println("======================");
		System.out.println("REGIONS");
		System.out.println("======================");
		for(int i = 0; i < regions.size(); i++){
			Region cur = regions.get(i);
			System.out.println("region: " + i);
			System.out.print("row range: "+cur.getRowInit()+"-"+cur.getRowEnd());
			System.out.println(", col range: "+cur.getColInit()+"-"+cur.getColEnd());
			
		}
		
		//////////////////////////
		// Save region assignment
		Path outpath = new Path(outputDir + "/regions");
		if(fs.exists(outpath))
			fs.delete(outpath, true);
		NullWritable nullkey = NullWritable.get();
		SequenceFile.Writer writer = new SequenceFile.Writer(fs, conf, outpath, NullWritable.class, Region.class);
		for(int i = 0; i < regions.size(); i++)
			writer.append(nullkey, regions.get(i));
		writer.close();
	}
}
