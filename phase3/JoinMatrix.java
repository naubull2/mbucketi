package phase3;

import java.util.ArrayList;

import utils.ColumnBucket;
import utils.JoinPredicate;
import utils.Quantile;
import utils.Region;

public class JoinMatrix {
	private Quantile[] columns;
	private Quantile[] rows;
	private ColumnBucket[] buckets;
	private JoinPredicate predicate;
	
	private long nCandidates;
	private int nRows;
	private int nCols;
	
	public JoinMatrix(Quantile[] columns, Quantile[] rows, JoinPredicate predicate){
		this.columns = columns;
		this.rows = rows;
		this.predicate = predicate;
		nCandidates = nRows = nCols = 0;
	}
	
	public void buildColumnBucket(){
		// Iterate through all rows, finding the total range of candidate cells
		// according to the join predicate
		buckets = new ColumnBucket[columns.length];
		int current_col_idx = 0;
		for(int col = 0; col < columns.length; col++){
			int current_row_idx = 0;
			boolean r_init = true;
			int r_start = 0;
			int r_end = 0;
			
			buckets[col] = new ColumnBucket();
			buckets[col].initialize(current_col_idx, current_col_idx);
			for(int row = 0; row < rows.length; row++){
				if(predicate == JoinPredicate.EQUI){
					if(columns[col].getMin() <= rows[row].getMax() && rows[row].getMin() <= columns[col].getMax()){
						// candidate intersection
						if(r_init){
							r_start = current_row_idx;
							r_end = r_start;
							r_init = false;
						}
						r_end += rows[row].getCount();
					}
				}
				else{	// LEQ
					if(rows[row].getMin() <= columns[col].getMax()){
						if(r_init){
							r_start = current_row_idx;
							r_end = r_start;
							r_init = false;
						}
						r_end += rows[row].getCount();
					}					
				}
				current_row_idx += rows[row].getCount();
			}
			nRows = current_row_idx;
			current_col_idx += columns[col].getCount();
			buckets[col].updateBucket(r_start, r_end-1, current_col_idx-1);
			nCandidates += (buckets[col].cMaxIdx - buckets[col].cMinIdx) *
					       (buckets[col].rMaxIdx - buckets[col].rMinIdx);
		}
		nCols = current_col_idx;
		///////////////////////////////////Print candidate cells///////////
		System.out.println("======================");
		System.out.println("Candidate Cells");
		System.out.println("======================");
		for(int i = 0; i < buckets.length; i++){
			ColumnBucket bucket = buckets[i];
			System.out.print("row range: "+bucket.rMinIdx+"-"+bucket.rMaxIdx);
			System.out.println(", col range: "+bucket.cMinIdx+"-"+bucket.cMaxIdx);
		}
	}
	
	public ArrayList<Region> MBucketI(int maxRegions){

		ArrayList<Region> regions = new ArrayList<Region>();
		int min_maxInput = (int)Math.ceil(2*Math.sqrt(nCandidates/maxRegions));
		int current_maxInput = min_maxInput;
		int max_maxInput = nRows + nCols; 

		System.out.println("======================");
		System.out.println("BINARY SEARCH");
		System.out.println("======================");
		// Binary search on maxInput to find the maxInput that utilize most of the given reducers
		while(max_maxInput > min_maxInput + 1) {
			current_maxInput = (min_maxInput + max_maxInput) / 2;
			System.out.println("range: "+min_maxInput+"~"+max_maxInput);
			System.out.println("target: "+current_maxInput);
			ArrayList<Region> temp = MBI(current_maxInput, maxRegions);
			if(temp == null){	// exceeds given maxRegions
				min_maxInput = current_maxInput;
				System.out.println("FAIL");
				System.out.println("");
			}else {
				max_maxInput = current_maxInput;
				if(regions.isEmpty() || regions.size() <= temp.size())
					regions = temp;
				System.out.println("SUCCESS");
				System.out.println("# of regions : "+regions.size());
				System.out.println("");
			}
		}
		return regions;
	}
	/**
	 * Finds a mapping of reducers with the given maxInput value
	 * @param maxInput
	 * @param maximum regions provided (maximum reducers allowed)
	 * @return array list of regions
	 */
	public ArrayList<Region> MBI(int maxInput, int maxRegions){
		ArrayList<Region> tmpRegions = new ArrayList<Region>();
		int sRowIndex = 0;
		while(sRowIndex < nRows){
			SubMatrixResult submatrix_rslt = coverSubMatrix(sRowIndex, maxInput);
			if(submatrix_rslt.regionList == null){	// nothing in the row
				sRowIndex += maxInput;
				continue;
			}
			tmpRegions.addAll(submatrix_rslt.regionList);
			if(tmpRegions.size() > maxRegions) {
				//System.out.println("reducer size on fail : "+tmpRegions.size());
				return null;
			}
			sRowIndex = submatrix_rslt.bestRow + 1;	// next sRow index 
		}
		return tmpRegions;
	}

	/**
	 * Finds the row covering with the highest score.
	 * Score is given as number of candidates covered / number of regions used
	 * @param starting row index
	 * @param maxInput
	 * @return regions used and the best covered last row index
	 */
	public SubMatrixResult coverSubMatrix(int sRow, int maxInput){
		double maxScore = 0;
		ArrayList<Region> finalRegions = null;
		int bestrow = 0;
		double upperBound = 0;
		for(int i = 0; i < maxInput && sRow + i <= nRows; i++){
			// Stop when upper bound is less than maxScore
			upperBound = (double)(i+1)*(maxInput-i-1);
			if(upperBound<maxScore){
			//	System.out.println("PRUNED______________________________________ Saved "+(maxInput-i-1)+" iterations");
				break;
			}
			ArrayList<Region> tmpRegions = coverRows(sRow, sRow + i , maxInput);
			if(tmpRegions.isEmpty())
				continue;
			long nCandidatesCovered = 0;
			for(Region r : tmpRegions)
				nCandidatesCovered += r.getCapacity();
			double score = (double) nCandidatesCovered / tmpRegions.size();
			if(maxScore <= score){
				maxScore = score;	
				finalRegions = tmpRegions;
				bestrow = i;
			}
		}
		return new SubMatrixResult(finalRegions, sRow + bestrow);
	}
	
	public ArrayList<Region> coverRows(int startRow, int endRow, int maxInput){
		ArrayList<Region> list = new ArrayList<Region>();
		int current_height = 0;
		int p_rows = 0;
		int p_rowf = 0;
		int cut_width = 0;
		int extension = 0;
		Region current_region = null;
		
		for(int col = 0; col < buckets.length; col++){
			int current_col = buckets[col].cMinIdx;
 			int width = buckets[col].cMaxIdx - buckets[col].cMinIdx + 1;
			int rows = Math.max(startRow, buckets[col].rMinIdx);
			int rowf = Math.min(endRow, buckets[col].rMaxIdx);
			
			if ( rows > rowf ){
				if(current_region!=null){
					list.add(current_region);
					current_region = null;
					extension = 0;
				}
				continue; //since this is a pruned area
			}
			current_height = rowf - rows + 1;
			// If we can extend from prior bucket.
			if (extension > 0){
				if( ((Math.max(p_rowf, rowf)-Math.min(p_rows, rows)+ 1) + Math.min(cut_width,(width+cut_width-extension))) <= maxInput){
					if(extension >= width){	// if the entire bucket can be covered
						extension -= width;
						current_region.updateArea(rows, buckets[col].cMinIdx, rowf, buckets[col].cMaxIdx);
						if(col == buckets.length-1){
							list.add(current_region);
							current_region = null;
						}
						continue;
					}else{	// if part of the bucket can be covered
						width -= extension;
						current_region.updateArea(rows, buckets[col].cMinIdx, rowf, buckets[col].cMinIdx+extension-1);
						current_col += extension;
						list.add(current_region);
						current_region = null;
						extension = 0;
					}
				}else{	// Don't extend if exceeds maxInput
					extension = 0;
					list.add(current_region);
					current_region = null;
				}
			}
			// cut the current bucket with the width / (max input - rows)
			cut_width = maxInput - current_height;
			if(cut_width == 0)
				continue; 	// exceeds maxInput
			while(width >= cut_width){
				current_region = new Region(rows, current_col, rowf, current_col+cut_width-1);
				width -= cut_width;
				list.add(current_region);
				current_region = null;
				current_col += cut_width;
			}
			if(current_col<=buckets[col].cMaxIdx){	// If we still have some left
				extension = cut_width - width;
				p_rows = rows;
				p_rowf = rowf;
				current_region = new Region(rows, current_col, rowf, current_col+width-1);
				if(extension == 0 || col+1 == buckets.length){
					list.add(current_region);
					current_region = null;
				}
			}
		}
		return list;
	}
	
	
	class SubMatrixResult{
		public int bestRow;
		public ArrayList<Region> regionList;
		
		public SubMatrixResult(ArrayList<Region> list, int rows){
			bestRow = rows;
			regionList = list;
		}
	}
	
	/* Test print join matrix on small data (100*100)
	public void printMatrix(int num_rows, int num_cols) throws FileNotFoundException{
		PrintStream ps = new PrintStream("matrix.txt");
		for(int row = 0; row < num_rows; row++){	// for each row,
			for(int col = 0; col < buckets.length; col++){	// iterate all column buckets and print accordingly
				char output = '0';
				if(buckets[col].rMinIdx <= row && buckets[col].rMaxIdx >= row){
					output = '1';
				}
				for(int i = 0; i<(buckets[col].cMaxIdx - buckets[col].cMinIdx); i++){
					ps.printf("%c", output);				
				}
			}
			ps.print("\n");
		}
		System.out.println("Finished printing matrix");
	}
	*/
}
