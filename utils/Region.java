package utils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class Region implements Writable{
	private int capacity;
	// inclusive bounds
	private int row_init;
	private int col_init;
	private int row_end;
	private int col_end;
	
	public Region(){
		capacity = 0;
	}
	
	public Region(Region r){
		capacity = r.capacity;
		row_init = r.row_init;
		col_init = r.col_init;
		row_end = r.row_end;
		col_end = r.col_end;
	}
	
	public Region(int row_init, int col_init, int row_end, int col_end){
		this.row_init = row_init;
		this.col_init = col_init;
		this.row_end = row_end;
		this.col_end = col_end;
		this.capacity =  (row_end-row_init + 1)*(col_end-col_init + 1);
	}
	public void updateArea(int sRow, int sCol, int eRow, int eCol){
		row_init = Math.min(row_init, sRow);
		row_end = Math.max(row_end, eRow);
		col_end = eCol;
		capacity += (eRow - sRow + 1) * (eCol - sCol + 1); 
	}
	
	public int getArea(){
		return (row_end - row_init + 1) * (col_end - col_init + 1);
	}
	
	public int getCapacity(){
		return capacity;
	}
	
	public int getRowInit(){
		return row_init;
	}
	
	public int getColInit(){
		return col_init;
	}
	
	public int getRowEnd(){
		return row_end;
	}
	
	public int getColEnd(){
		return col_end;
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		capacity = in.readInt();
		row_init = in.readInt();
		col_init = in.readInt();
		row_end = in.readInt();
		col_end = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(capacity);
		out.writeInt(row_init);
		out.writeInt(col_init);
		out.writeInt(row_end);
		out.writeInt(col_end);
	}
}
