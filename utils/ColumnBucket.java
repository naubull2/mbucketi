package utils;

public class ColumnBucket {
	public int rMinIdx;			// beginning row index
	public int rMaxIdx;			// last row index
	public int cMinIdx;			// beginning column index
	public int cMaxIdx; 			// last column index
		
	public ColumnBucket(){
		rMinIdx = Integer.MAX_VALUE;
		rMaxIdx = Integer.MIN_VALUE;
		cMinIdx = Integer.MAX_VALUE;
		cMaxIdx = Integer.MIN_VALUE;
	}
	
	public void initialize(int cMin, int cMax){
		cMinIdx = cMin;
		cMaxIdx = cMax;
	}
	
	public void updateBucket(int rMin, int rMax, int cMax){
		rMinIdx = rMin;
		rMaxIdx = rMax;
		cMaxIdx = cMax;
	}	
}
