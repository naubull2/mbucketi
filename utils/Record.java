package utils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;


public class Record implements WritableComparable<Record> {
	private char 	table;
	private int 	id;
	private int 	value;
	
	public Record(){}
	
	public Record(Record r){
		this.table = r.table;
		this.id = r.id;
		this.value = r.value;
	}
	
	public Record(char table, int id, int value){
		this.table = table;
		this.id = id;
		this.value = value;
	}
	
	public Record(String str){
		String[] arrStr = str.split("\t");
		this.table = arrStr[0].charAt(0);
		this.id = Integer.parseInt(arrStr[1]);
		this.value = Integer.parseInt(arrStr[2]);
	}
	
	public void set(Record o){
		this.table = o.table;
		this.id = o.id;
		this.value = o.value;
	}
	
	public void set(char table, int id, int value) {
		this.table = table;
		this.id = id;
		this.value = value;
	}

	public String toString()
	{
		return String.format("%c\t%d\t%d", table,id,value);
	}
	
	public char getTable(){
		return table;
	}
	
	public int getId(){
		return id;
	}
	
	public int getValue(){
		return value;
	}
	
	@Override
	public void readFields(DataInput in) throws IOException{
		table = in.readChar();
		id = in.readInt();
		value = in.readInt();
	}
	
	@Override
	public void write(DataOutput out) throws IOException {
		out.writeChar(table);
		out.writeInt(id);
		out.writeInt(value);
	}
	
	@Override
	public int compareTo(Record other) {
		int cmp = Integer.compare(value, other.value);
		if(cmp == 0) return Integer.compare(id, other.id);
		return cmp;
	}
}