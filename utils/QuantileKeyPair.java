package utils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class QuantileKeyPair implements WritableComparable<QuantileKeyPair> {
	private int realKey = 0;
	private int value = 0;
	
	public void set(int key, int value) {
		realKey = key;
		this.value = value;
	}

	public String toString()
	{
		return String.format("%d\t%d", realKey, value);
	}

	public char getRealKey() {
		return realKey==0?'R':'S';
	}
	public int getValue() {
		return value;
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		realKey = in.readInt();
		value = in.readInt();
	}
	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(realKey);
		out.writeInt(value);
	}
	@Override
	public int hashCode() {
		return realKey * 127;
	}
	@Override
	public boolean equals(Object right) {
		if (!(right instanceof QuantileKeyPair))
			return false;
		QuantileKeyPair r = (QuantileKeyPair) right;
		return r.realKey == realKey && r.value == value;
	}
	
	
	public static class Comparator extends WritableComparator {
		public Comparator() {
			super(QuantileKeyPair.class);
		}

		public int compare(byte[] b1, int s1, int l1,
				byte[] b2, int s2, int l2) {
			return compareBytes(b1, s1, l1, b2, s2, l2);
		}
	}

	static {  
		WritableComparator.define(QuantileKeyPair.class, new Comparator());
	}

	@Override
	public int compareTo(QuantileKeyPair other) {
		if (realKey != other.realKey) {
			return realKey < other.realKey ? -1 : 1;
		} else if (value != other.value) {
			return value < other.value ? -1 : 1;
		} else {
			return 0;
		}
	}
}