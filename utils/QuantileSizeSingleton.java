package utils;

public class QuantileSizeSingleton {
	private int rSize;
	private int sSize;
	
    private static QuantileSizeSingleton instance;
     
    private QuantileSizeSingleton(){}
     
    public static synchronized QuantileSizeSingleton getInstance(){
        if(instance == null){
            instance = new QuantileSizeSingleton();
        }
        return instance;
    }
    public void setR(int r){
    	rSize = r;
    }
    public void setS(int s){
    	sSize = s;
    }
    
    public int getR(){
    	return rSize;
    }
    public int getS(){
    	return sSize;
    }
}
