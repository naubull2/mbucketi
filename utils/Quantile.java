package utils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;


public class Quantile implements Writable {
	private int 	MIN;
	private int 	MAX;
	private int 	count;
	
	// Initialize values
	public Quantile(){
		MIN = Integer.MAX_VALUE;
		MAX = Integer.MIN_VALUE;
		count = 0;
	}
	
	public Quantile(Quantile q){
		this.MIN = q.MIN;
		this.MAX = q.MAX;
		this.count = q.count;
	}
	
	public String toString(){
		return String.format("%d\t%d\t%d", MIN, MAX, count);
	}
	
	public void addRecord(Record r){
		int val = r.getValue();
		if(val < MIN)
			MIN = val;
		if(val > MAX)
			MAX =val;
		count += 1;
	}
	
	public int getMin(){
		return MIN;
	}
	public int getMax(){
		return MAX;
	}
	public int getCount(){
		return count;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		MIN = in.readInt();
		MAX = in.readInt();
		count = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(MIN);
		out.writeInt(MAX);
		out.writeInt(count);
	}
}