package utils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class BucketKeyPair implements WritableComparable<BucketKeyPair>{
	private char table;
	private int bucketId;
	
	public BucketKeyPair(){}
	
	public BucketKeyPair(char table, int id){
		this.table = table;
		this.bucketId = id;
	}
	
	public void set(char table, int id){
		this.table = table;
		bucketId = id;
	}
	
	public BucketKeyPair(String str){
		String[] strArray = str.split("[ |\t]+");
		table = strArray[0].charAt(0);
		bucketId = Integer.parseInt(strArray[1]);
	}
	
	public String toString(){
		return String.format("%c\t%d", table, bucketId);
	}
	
	public char getTable(){
		return table;
	}
	
	public int getBucketId(){
		return bucketId;
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		table = in.readChar();
		bucketId = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeChar(table);
		out.writeInt(bucketId);
	}

	@Override
	public int compareTo(BucketKeyPair o) {
		int cmp = Character.compare(table, o.table);
		if(cmp==0)
			return Integer.compare(bucketId, o.bucketId);
		return cmp;
	}

	@Override
	public int hashCode() {
		Integer i = new Integer(bucketId);
		Character c = new Character(table);
		return i.hashCode() + c.hashCode();
	}
	
	

}
