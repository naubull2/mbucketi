package wilee;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.GenericOptionsParser;
import phase1.Phase1Driver;
import phase2.Phase2Driver;
import phase3.Phase3Driver;
import phase4.Phase4Driver;
import utils.JoinPredicate;
import utils.QuantileSizeSingleton;

public class MBucketDriver {
	/*
	 * 1. Input file
	 * 2. Output file
	 * 3. Join predicate
	 * 4. k quantile number
	 * 5. |R|
	 * 6. |S|
	 * 7. Number of samples from R
	 * 8. Number of samples from S
	 * 9. Desired number of regions
	 * 10. Number of reducers
	 */
	public static void main(String[] args) throws Exception{
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		if(otherArgs.length != 9){
			printHelp();
			return;
		}
		String inFile = otherArgs[0];
		String outFile = otherArgs[1];
		JoinPredicate predicate = otherArgs[2].equals("equi")? JoinPredicate.EQUI:JoinPredicate.LEQ;
		int numQuantiles = Integer.parseInt(otherArgs[3]);
		int rSize = Integer.parseInt(otherArgs[4]);
		int sSize = Integer.parseInt(otherArgs[5]);
		int rSamples = Integer.parseInt(otherArgs[6]);
		int sSamples = Integer.parseInt(otherArgs[7]);
		int numRegions = Integer.parseInt(otherArgs[8]);
		
		long startTime = System.currentTimeMillis();
		// Phase 1
		// Sample from R,S and choose the k-quantile values
		String phase1OutDir = "phase1";
		Phase1Driver.run(args, inFile, phase1OutDir, numQuantiles, rSize, sSize, rSamples, sSamples);
		long sampleTime = System.currentTimeMillis();
		// Phase 2
		// Count each bucket and build histogram
		String phase2OutDir = "phase2";
		Phase2Driver.run(args, inFile, phase1OutDir, phase2OutDir);
		long histogramTime = System.currentTimeMillis();
		// Phase 3
		// Compute Join-matrix for partitioning
		QuantileSizeSingleton size = QuantileSizeSingleton.getInstance();
		String phase3OutDir = "phase3";
		Phase3Driver.run(phase2OutDir, phase3OutDir, size, numRegions, predicate);
		long mbiTime = System.currentTimeMillis();
		// Phase 4
		// Do the actual join according to the region assignments
		Phase4Driver.run(args, phase2OutDir, phase3OutDir, inFile, outFile, size, numRegions, predicate);
		long joinTime = System.currentTimeMillis();
		
		System.out.println("Sampling time : "+(sampleTime-startTime)/1000.0f+"sec");
		System.out.println("Building histogram time : "+(histogramTime-sampleTime)/1000.0f+"sec");
		System.out.println("Running MBI time : "+(mbiTime-histogramTime)/1000.0f+"sec");
		System.out.println("Joining time : "+(joinTime-mbiTime)/1000.0f+"sec");
		System.out.println("Total execution time : "+(joinTime-startTime)/1000.0f+"sec");
	}
	public static void printHelp(){
		System.out.println("Usage : [input file] [output file] [leq|equi] [# quantiles k] [# R] [# S]");
		System.out.println("        [# samples R] [# samples S] [# regions]");
	}
}
