package phase2;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import utils.BucketKeyPair;
import utils.Record;

public class Phase2Mapper extends Mapper<LongWritable, Text, BucketKeyPair, Record > {
	private ArrayList<Integer>	quantileR;
	private ArrayList<Integer>	quantileS;
	private BucketKeyPair emitKey;
	private Record emitValue;
	/*
	 * Read phase 1 output sequence file
	 */
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		Configuration conf = context.getConfiguration();
		URI[] uris = DistributedCache.getCacheFiles(conf);
		FileSystem fs = FileSystem.get(conf);
		SequenceFile.Reader reader = new SequenceFile.Reader(fs, new Path(uris[0].toString()), conf);
		Text key = new Text();
		IntWritable val = new IntWritable();
		
		quantileR = new ArrayList<Integer>();
		quantileS = new ArrayList<Integer>();
		emitKey = new BucketKeyPair();
		
		quantileR.add(0);
		quantileS.add(0);
		
		while(reader.next(key, val)) {
			if(key.toString().equals("R"))
				quantileR.add(val.get());
			else
				quantileS.add(val.get());
		}
		reader.close();
	}

	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		// search from back to front
		Record currentRecord = new Record(value.toString());
		ArrayList<Integer> quantile = (currentRecord.getTable()=='R')? quantileR : quantileS;
		int i;
		
		for(i = quantile.size()-1; i >= 0; i--){
			if(quantile.get(i) <= currentRecord.getValue()){
				emitKey.set(currentRecord.getTable(), i);
				emitValue = new Record(currentRecord);
				break;	// then bucket id == i
			}
		}
		context.write(emitKey, emitValue);
	}
}