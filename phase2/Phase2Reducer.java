package phase2;

import java.io.IOException;

import org.apache.hadoop.mapreduce.Reducer;

import utils.BucketKeyPair;
import utils.Quantile;
import utils.Record;

public class Phase2Reducer extends Reducer<BucketKeyPair, Record, BucketKeyPair, Quantile>{
	private Quantile emitValue;
	
	@Override
	protected void reduce(BucketKeyPair key, Iterable<Record> values, Context context) throws IOException, InterruptedException {
		emitValue = new Quantile();
		for(Record val : values)
		{
			emitValue.addRecord(val);
		}
		context.write(key, emitValue);
	}	
}
