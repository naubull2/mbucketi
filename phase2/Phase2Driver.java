package phase2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import utils.BucketKeyPair;
import utils.Quantile;
import utils.Record;

public class Phase2Driver {
	/**
	 * @param inputDir input directory
	 * @param phase1Dir phase1 output directory
	 * @param outputDir output directory
	 * @param numReducers number of desired reducers
	 * @throws Exception
	 */
	public static void run(String[] args, String inputDir, String phase1Dir, String outputDir) throws Exception{
		Configuration conf = new Configuration();
		new GenericOptionsParser(conf, args);
		FileSystem fs = FileSystem.get(conf);
		fs.delete(new Path(outputDir), true);
		
		// Access phase1 sampled data from each slaves
		DistributedCache.addCacheFile(new Path(phase1Dir + "/part-r-00000").toUri(), conf);
		Job job = Job.getInstance(conf, "Phase2:Count buckets");
		job.setJarByClass(phase1.Phase1Driver.class);
		job.setMapperClass(Phase2Mapper.class);
		job.setMapOutputKeyClass(BucketKeyPair.class);
		job.setMapOutputValueClass(Record.class);
		
		job.setReducerClass(Phase2Reducer.class);
		job.setOutputKeyClass(BucketKeyPair.class);
		job.setOutputValueClass(Quantile.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);
		
		FileInputFormat.setInputPaths(job, new Path(inputDir));
		FileOutputFormat.setOutputPath(job, new Path(outputDir));
		if(!job.waitForCompletion(true)) return;
	}
}
