package phase1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import utils.QuantileKeyPair;

public class Phase1Reducer extends Reducer<QuantileKeyPair, IntWritable, Text, IntWritable>{
	private IntWritable emitValue;
	private Text emitKey;
	private ArrayList<Integer> vals;
	private int k;
	private int Rsample, Ssample;
	
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		Configuration config = context.getConfiguration();
		k = config.getInt("quantiles", 3);
		Rsample = config.getInt("rSamples", 3);
		Ssample = config.getInt("sSamples", 3);
		emitValue = new IntWritable();
		emitKey = new Text();
		vals = new ArrayList<Integer>();
	}

	public void reduce(QuantileKeyPair key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		// emit k-quantiles
		int size = 0;
		int cnt = 0;
		int value = 0;
		for(IntWritable val:values){
			vals.add(val.get());
		}
		HashSet hs = new HashSet();
		hs.addAll(vals);
		vals.clear();
		vals.addAll(hs);
		Collections.sort(vals);
		size = vals.size();
		for(int i = 1; i < ((size>k)? k:size); i++){
			int index = size>k? (int)(i*size)/k : i;	// In case too many duplicates are sampled
			emitKey.set(String.valueOf(key.getRealKey()));
			emitValue.set(vals.get(index));
			context.write(emitKey, emitValue);
		}
		vals.clear();
	}
}
