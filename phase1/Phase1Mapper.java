package phase1;

import java.io.IOException;
import java.util.Random;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import utils.QuantileKeyPair;

public class Phase1Mapper extends Mapper<Object, Text, QuantileKeyPair, IntWritable>{
	private QuantileKeyPair emitKey;
	private IntWritable emitValue;
	private double Rsize, Ssize, Rsample, Ssample;
	private double ratio;
	
	
	@Override
	protected void setup(Context context) throws IOException, InterruptedException{
		Configuration config = context.getConfiguration();
		Rsize = config.getInt("rSize", 10);
		Ssize = config.getInt("sSize", 10);
		Rsample = config.getInt("rSamples", 3);
		Ssample = config.getInt("sSamples", 3);
		emitKey = new QuantileKeyPair();
		emitValue = new IntWritable();
	}
	
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException{
		StringTokenizer token = new StringTokenizer(value.toString());
		Random rand = new Random();
		
		String origin = token.nextToken();
		ratio = origin.equals("R")? Rsample/Rsize : Ssample/Ssize;
					
		// emit key value with a probability of sampleSize/totalSize
		if(rand.nextDouble() <= ratio){
			token.nextToken();	// record id is not needed here
			int actualValue = Integer.parseInt(token.nextToken());
			emitKey.set(origin.equals("R")? 0:1, actualValue);	// composite key for secondary sorting
			emitValue.set(actualValue);
			context.write(emitKey, emitValue);
		}	
	}
}