package phase1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.lib.MultipleOutputs;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import utils.QuantileKeyPair;

public class Phase1Driver {
	
	public static class QuantileKeyGroupingComparator 
	implements RawComparator<QuantileKeyPair> {
		@Override
		public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
			return WritableComparator.compareBytes(b1, s1, Integer.SIZE/8, 
					b2, s2, Integer.SIZE/8);
		}

		@Override
		public int compare(QuantileKeyPair o1, QuantileKeyPair o2) {
			int l = o1.getRealKey();
			int r = o2.getRealKey();
			return l == r ? 0 : (l < r ? -1 : 1);
		}
	}

	public static class QuantileKeyPartitioner extends Partitioner<QuantileKeyPair, IntWritable>{
		@Override
		public int getPartition(QuantileKeyPair key, IntWritable value, 
				int numPartitions) {
			return Math.abs(key.getRealKey() * 127) % numPartitions;
		}
	}
	/**
	 * @param main arguments for -jt local option
	 * @param inputfile
	 * @param outputdir
	 * @param k quantiles
	 * @param size of R
	 * @param size of S
	 * @param sample size of R
	 * @param sample size of S
	 * @throws Exception
	 */
	public static void run(String[] args, String inputfile, String outputdir, int quantiles, int rSize, int sSize, int rSamples, int sSamples) throws Exception {
		Configuration conf = new Configuration();
		new GenericOptionsParser(conf, args);
		FileSystem fs = FileSystem.get(conf);
		fs.delete(new Path(outputdir), true);

		conf.setInt("quantiles", quantiles);
		conf.setInt("rSize", rSize);
		conf.setInt("sSize", sSize);
		conf.setInt("rSamples", rSamples);
		conf.setInt("sSamples", sSamples);

		// Phase 1
		Job job = Job.getInstance(conf, "Phase1:Sampling");
		job.setJarByClass(phase1.Phase1Driver.class);
		job.setMapperClass(Phase1Mapper.class);
		job.setReducerClass(Phase1Reducer.class);
		
		job.setMapOutputKeyClass(QuantileKeyPair.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		job.setOutputFormatClass(SequenceFileOutputFormat.class);
		
		// secondary sorting comparator & partitioner class
		job.setGroupingComparatorClass(QuantileKeyGroupingComparator.class);
	    job.setPartitionerClass(QuantileKeyPartitioner.class);
	    
		job.setNumReduceTasks(2);
		FileInputFormat.setInputPaths(job, new Path(inputfile));
		FileOutputFormat.setOutputPath(job, new Path(outputdir));
		if(!job.waitForCompletion(true)) return;
	}
}
